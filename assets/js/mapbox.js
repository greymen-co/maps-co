$(function () {
    var $map = $('#map');
    if (!$map.length) {
        return;
    }

    var mapIsLoaded = false,
        createObserver = function (target, callback) {
            new IntersectionObserver(function (entries) {
                observerCallback(entries, callback);
            }).observe(target);
        },
        observerCallback = function (entries, callback) {
            entries.forEach(entry => {
                if (entry.isIntersecting) {
                    callback();
                }
            });
        },
        loadJS = function (FILE_URL, isAsync, callbackLoaded, callbackError) {
            var element = document.createElement('script');
            element.setAttribute('src', FILE_URL);
            element.setAttribute('type', 'text/javascript');
            if (isAsync) {
                element.setAttribute('async', 'true');
            }
            element.addEventListener('load', callbackLoaded);
            element.addEventListener('error', callbackError);
            document.body.appendChild(element);
        },
        loadCSS = function (FILE_URL, callbackLoaded) {
            var element = document.createElement('link');
            element.setAttribute('href', FILE_URL);
            element.setAttribute('rel', 'stylesheet');
            element.addEventListener('load', callbackLoaded);
            document.body.appendChild(element);
        },
        loadMap = function () {
            // Map is already loaded. Do nothing.
            if (mapIsLoaded) {
                return;
            }

            mapIsLoaded = true;

            var mapboxSettings = $map.data().mapboxSettings,
                initMap = function () {
                    mapboxgl.accessToken = mapboxSettings.api_key;

                    var mapsData = $map.data().mapsData,
                        mapData,
                        map = new mapboxgl.Map({
                            container: 'map',
                            style: mapboxSettings.style_url,
                            center: [5.6, 52.2],
                            logoPosition: 'top-right',
                            attributionControl: false,
                            minZoom: 6.5,
                            zoom: 6,
                        }),
                        spiderifier = null,
                        nav = new mapboxgl.NavigationControl(),
                        currentPopup = null,
                        clickedMarkerId = null,
                        searchDisabled = mapboxSettings.search_disabled,
                        isSearching = false;

                    // Load Spiderifier
                    if (mapboxSettings.spiderifier_enabled) {
                        spiderifier = new MapboxglSpiderifier(map, {
                            animate: true,
                            animationSpeed: 200,
                            onClick: function (e, marker) {
                                createPopup(marker.feature, marker.mapboxMarker._lngLat, MapboxglSpiderifier.popupOffsetForSpiderLeg(marker))
                            }
                        });
                    }

                    map.on('style.load', function () {
                        map.addSource('pins', {
                            type: 'geojson',
                            data: null,
                            cluster: true,
                            clusterMaxZoom: 16,
                            clusterRadius: 50,
                        });

                        map.loadImage(mapboxSettings.icons.marker, function (err, image) {
                            if (err) {
                                throw err;
                            }
                            map.addImage('map-marker', image);
                        });

                        map.loadImage(mapboxSettings.icons.marker_clusterer.icon, function (err, image) {
                            if (err) {
                                throw err;
                            }
                            map.addImage('map-marker-clusterer', image);
                        });

                        map.addLayer({
                            id: 'pins',
                            type: 'symbol',
                            source: 'pins',
                            layout: {
                                'icon-image': 'map-marker',
                                'icon-size': 0.5,
                            },
                            filter: ['all', ['!has', 'point_count']],
                        });

                        map.addLayer({
                            id: 'cluster-pins',
                            type: 'symbol',
                            source: 'pins',
                            filter: ['all', ['has', 'point_count']],
                            layout: {
                                'icon-image': 'map-marker-clusterer',
                                'icon-size': 0.6,
                                'text-field': '{point_count_abbreviated}',
                                'text-size': 14,
                                'text-offset': [Number(mapboxSettings.icons.marker_clusterer.offsets.x), Number(mapboxSettings.icons.marker_clusterer.offsets.y)],
                            },
                            paint: {
                                "text-color": '#FFFFFF'
                            },
                        });

                        map.scrollZoom.disable();
                        map.addControl(nav, 'bottom-right');
                        map.resize();

                        map.on('click', function () {
                            map.scrollZoom.enable();
                        });

                        map.on('idle', function () {
                            var zoomLevel = map.getZoom(),
                                center = map.getCenter(),
                                distance = 0;

                            if (zoomLevel > 7) {
                                if (zoomLevel < 8) {
                                    distance = 125;
                                } else if (zoomLevel < 9) {
                                    distance = 50;
                                } else if (zoomLevel < 10) {
                                    distance = 40;
                                } else if (zoomLevel < 12) {
                                    distance = 15;
                                } else {
                                    distance = 10;
                                }

                                if (!searchDisabled && !isSearching) {
                                    isSearching = true;
                                    $.request('onSearch', {
                                        data: {
                                            latitude: center.lat,
                                            longitude: center.lng,
                                            searchRadius: distance,
                                            id: clickedMarkerId,
                                        }
                                    });
                                }
                            }
                        });

                        map.on('moveend', function () {
                            isSearching = false;
                        });

                        // Enable popups for markers
                        if (!mapboxSettings.popup_disabled) {
                            map.on('click', 'pins', function (e) {
                                var features = map.queryRenderedFeatures(e.point, {layers: ['pins']});
                                if (!features.length) {
                                    return;
                                }
                                map.flyTo({
                                    center: features[0].geometry.coordinates,
                                });
                                createPopup(features[0].properties, features[0].geometry.coordinates);
                            });
                            map.on('click', 'cluster-pins', clickOnCluster);
                        }

                        getGeoJSON();
                    });

                    function clickOnCluster(e) {
                        console.log('clickOnCluster');
                        var features = map.queryRenderedFeatures(e.point, {layers: ['cluster-pins']});

                        if (!features.length && spiderifier) {
                            spiderifier.unspiderfy();
                            return;
                        }

                        var maxZoom = (spiderifier) ? 10 : 16;

                        if (map.getZoom() < maxZoom) {
                            map.easeTo({center: e.lngLat, zoom: map.getZoom() + 2});
                            return;
                        }

                        if (spiderifier) {
                            map.getSource('pins').getClusterLeaves(
                                features[0].properties.cluster_id,
                                100,
                                0,
                                function (err, leafFeatures) {
                                    if (err) {
                                        return console.error('error while getting leaves of a cluster', err);
                                    }
                                    var markers = _.map(leafFeatures, function (leafFeature) {
                                        return leafFeature.properties;
                                    });
                                    spiderifier.spiderfy(features[0].geometry.coordinates, markers);
                                }
                            );
                        }
                    }

                    function createPopup(data, coords, offset) {
                        closePopup();
                        if (!offset) {
                            offset = [0, 0];
                        }
                        currentPopup = new mapboxgl.Popup({
                            closeOnClick: false,
                            offset,
                        })
                            .setLngLat(coords)
                            .setHTML(data.popupHTML)
                            .addTo(map)
                            .on('close', function () {
                                currentPopup = null;
                                clickedMarkerId = null;
                                $('.list-group li').removeClass('active');
                            });
                        clickedMarkerId = data.id;
                        $('.list-group').find('[data-id=' + clickedMarkerId + ']').addClass('active');
                    }

                    function closePopup() {
                        if (!currentPopup) {
                            return;
                        }
                        currentPopup.remove();
                        currentPopup = null;
                        clickedMarkerId = null;
                    }

                    function centerMap() {
                        const params = new Proxy(new URLSearchParams(window.location.search), {
                            get: (searchParams, prop) => searchParams.get(prop),
                        });
                        if (!params.clong || !params.clat) {
                            return;
                        }
                        map.flyTo({
                            center: [params.clong, params.clat],
                            zoom: 9,
                        });
                    }

                    function getGeoJSON() {
                        $.request('onGeoJSON', {
                            data: {
                                mapsData,
                                mapboxSettings,
                            },
                            success: function (data) {
                                mapData = data;
                                setGeoJSON(mapData);
                            }
                        });
                    }

                    window.setGeoJSON = function (mapData) {
                        var pins = map.getSource('pins');
                        pins.setData(mapData);
                        centerMap();
                    }

                    function getCurrentLocation() {
                        if (navigator.geolocation) {
                            $('#my-location').addClass('oc-loading');
                            $('#my-location').html('fetching location');
                            navigator.geolocation.getCurrentPosition(goCurrentLocation);
                        } else {
                            $('#my-location').html('Geolocation is not supported by this browser.');
                        }
                    }

                    function goCurrentLocation(position) {
                        $('#my-location').removeClass('oc-loading');
                        html = '';
                        map.flyTo({
                            center: [position.coords.longitude, position.coords.latitude],
                            zoom: 14
                        });
                        $('#my-location').html(html);
                    }

                    function goCompanyLocation(obj) {
                        map.flyTo({
                            center: [$(obj).data('lng'), $(obj).data('lat')],
                            zoom: 14
                        });
                        var feature = mapData.features[$(obj).data('id') - 1];
                        createPopup(feature.properties, feature.geometry.coordinates);
                    }

                    function setLocationInForm(data) {
                        $('#search-stores-selection').addClass('list-group-item').show();
                        $('#store_id').val(data.location.id);
                        $('#store_name').val(data.location.name);

                        $('.search-stores__input-container, #search-stores-results').hide();
                        $('#s', '.search-stores__input-container').val('');
                        $('#search-stores-results').empty();

                        $('.search-stores__edit-location').click(removeLocationInForm);
                    }

                    function removeLocationInForm() {
                        $('#store_id, #store_name').val('');
                        $('#search-stores-selection').removeClass('list-group-item').hide().empty();
                        $('.search-stores__input-container, #search-stores-results').show();
                    }

                    function searchResult(data) {
                        var feature = data.feature ? JSON.parse(data.feature) : null;
                        if (feature) {
                            map.flyTo({
                                center: feature.center,
                                zoom: 12
                            });
                        }
                    }

                    function hideSearch(clean) {
                        if (clean) {
                            $('input[name=s]').val('');
                        }
                        centerMap();
                    }

                    $(document).on('ajaxSuccess', function (e, xhr) {
                        if (xhr.handler !== 'Vacancies::onSearch') {
                            return;
                        }
                        getGeoJSON();
                    });
                };

            loadCSS('//api.mapbox.com/mapbox-gl-js/' + mapboxSettings.version + '/mapbox-gl.css', function () {
                loadJS('//api.mapbox.com/mapbox-gl-js/' + mapboxSettings.version + '/mapbox-gl.js', false, initMap);
            });
        };

    createObserver($map[0], loadMap);
});