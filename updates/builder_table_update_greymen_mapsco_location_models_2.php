<?php namespace Greymen\MapsCo\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateGreymenMapscoLocationModels2 extends Migration
{
    public function up()
    {
        Schema::table('greymen_mapsco_location_models', function($table)
        {
            $table->string('popup_field', 100)->after('model_type')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('greymen_mapsco_location_models', function($table)
        {
            $table->dropColumn('popup_field');
        });
    }
}
