<?php namespace Greymen\MapsCo\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateGreymenMapscoLocations extends Migration
{
    public function up()
    {
        Schema::table('greymen_mapsco_locations', function($table)
        {
            $table->double('latitude', 16, 12)->after('model_type')->nullable();
            $table->double('longitude', 16, 12)->after('latitude')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('greymen_mapsco_locations', function($table)
        {
            $table->dropColumn('latitude');
            $table->dropColumn('longitude');
        });
    }
}
