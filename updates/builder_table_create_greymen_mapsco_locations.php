<?php namespace Greymen\MapsCo\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateGreymenMapscoLocations extends Migration
{
    public function up()
    {
        Schema::create('greymen_mapsco_locations', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('model_id')->nullable()->unsigned();
            $table->string('model_type', 255)->nullable();
            $table->text('data')->nullable();
            $table->index('model_id');
            $table->index('model_type');
        });
    }

    public function down()
    {
        Schema::dropIfExists('greymen_mapsco_locations');
    }
}