<?php namespace Greymen\MapsCo\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateGreymenMapscoLocationModels4 extends Migration
{
    public function up()
    {
        Schema::table('greymen_mapsco_location_models', function($table)
        {
            $table->string('excluded_ids', 255)->after('popup_field')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('greymen_mapsco_location_models', function($table)
        {
            $table->dropColumn('excluded_ids');
        });
    }
}
