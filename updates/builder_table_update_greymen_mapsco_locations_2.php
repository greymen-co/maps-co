<?php namespace Greymen\MapsCo\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateGreymenMapscoLocations2 extends Migration
{
    public function up()
    {
        Schema::table('greymen_mapsco_locations', function($table)
        {
            $table->json('data')->nullable()->unsigned(false)->default(null)->comment(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('greymen_mapsco_locations', function($table)
        {
            $table->text('data')->nullable()->unsigned(false)->default(null)->comment(null)->change();
        });
    }
}