<?php namespace Greymen\MapsCo\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateGreymenMapscoLocationModels extends Migration
{
    public function up()
    {
        Schema::table('greymen_mapsco_location_models', function($table)
        {
            $table->text('overwrite_fields')->after('model_type')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('greymen_mapsco_location_models', function($table)
        {
            $table->dropColumn('overwrite_fields');
        });
    }
}
