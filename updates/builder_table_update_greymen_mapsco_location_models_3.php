<?php namespace Greymen\MapsCo\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateGreymenMapscoLocationModels3 extends Migration
{
    public function up()
    {
        Schema::table('greymen_mapsco_location_models', function($table)
        {
            $table->string('detail_page_url', 100)->after('popup_field')->nullable();
            $table->string('view_page_label', 100)->after('popup_field')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('greymen_mapsco_location_models', function($table)
        {
            $table->dropColumn('detail_page_url');
            $table->dropColumn('view_page_label');
        });
    }
}
