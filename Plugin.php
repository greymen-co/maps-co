<?php namespace Greymen\MapsCo;

use Greymen\MapsCo\Controllers\LocationModels;
use Greymen\MapsCo\Models\LocationModel;
use Greymen\MapsCo\Models\Setting;
use RainLab\Pages\Classes\Page;
use System\Classes\PluginBase;
use System\Classes\SettingsManager;

/**
 * Class Plugin
 * @package Greymen\MapsCo
 */
class Plugin extends PluginBase
{
    public $require = ['Rainlab.Location'];

    public function registerComponents()
    {
        return [
            Components\Mapbox::class => 'Mapbox',
            Components\SearchStores::class => 'SearchStores',
        ];
    }

    /**
     * @return array[]
     */
    public function registerSettings()
    {
        return [
            'settings' => [
                'label' => 'greymen.mapsco::lang.plugin.name',
                'description' => 'greymen.mapsco::lang.plugin.description',
                'icon' => 'icon-cog',
                'category' => SettingsManager::CATEGORY_SYSTEM,
                'class' => Setting::class,
                'order' => 500,
                'keywords' => 'maps storelocator',
                'permissions' => ['greymen.mapsco']
            ]
        ];
    }

    public function registerMarkupTags()
    {
        return [
            'filters' => [
                'truncate_by_words' => [$this, 'truncateByWords'],
            ],
        ];
    }

    public function boot()
    {
        // Extends fields for all linked location models
        LocationModels::extendFields();

        // Add "location_model" options
        Page::extend(function ($model) {
            $model->addDynamicMethod('getLocationModelOptions', function () {
                return LocationModel::getModels();
            });
        });
    }

    /**
     * @param $input
     * @param $length
     * @param $moreText
     * @return string
     */
    public function truncateByWords($input, $length = 35, $moreText = ' ...')
    {
        $input = strip_tags($input);
        $words = preg_split("/[\n\r\t ]+/", $input, $length + 1, PREG_SPLIT_NO_EMPTY);
        if (count($words) > $length) {
            array_pop($words);
            $input = implode(' ', $words) . $moreText;
        } else {
            $input = implode(' ', $words);
        }
        return $input;
    }
}
