<?php namespace Greymen\MapsCo\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Greymen\MapsCo\Models\Location;
use Greymen\MapsCo\Models\LocationModel;
use October\Rain\Support\Facades\Event;
use Backend\Widgets\Form;

class LocationModels extends Controller
{
    public $implement = [
        'Backend\Behaviors\ListController',
        'Backend\Behaviors\FormController',
    ];
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $requiredPermissions = ['admin-locations'];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Greymen.Maps', 'main-menu-item', 'side-menu-locations');
    }

    public static function extendFields()
    {
        Event::listen('backend.form.extendFields', function (Form $widget) {
            if (!in_array(get_class($widget->model), LocationModel::getModels())) {
                return;
            }

            if ($widget->isNested) {
                return;
            }

            $widget->addFields([
                'location' => [
                    'label' => 'greymen.mapsco::lang.fields.locations.title',
                    'type' => 'nestedform',
                    'context'=> ['update'],
                    'form' => '$/greymen/mapsco/models/LOCATION_FIELDS.yaml'
                ],
            ], 'primary');
        });

        foreach (LocationModel::getModels() as $id => $name) {
            $locationModel = LocationModel::find($id);

            // TODO: Move functions to separate file
            $name::extend(function ($model) use ($locationModel) {
                $model->addJsonable('location');

                $model->addDynamicMethod('getLocations', function ($params = [], $settings = []) use ($model, $locationModel) {
                    $output = [];
                    $refreshMarkersAfterFilter = !(isset($settings['refresh_markers_after_filter'])) || boolval($settings['refresh_markers_after_filter']);
                    $limit = (!empty($params['limit'])) ? $params['limit'] : -1;
                    $where = [
                        ['model_type', '=', $locationModel->model_type],
                        ['latitude', '!=', ''],
                        ['longitude', '!=', '']
                    ];

                    // Search by location ID
                    if (!empty($params['location_id'])) {
                        $where[] = ['model_id', '=', $params['location_id']];
                    }

                    // Search by coordinates
                    if ($refreshMarkersAfterFilter && !empty($params['latitude']) && !empty($params['longitude'])) {
                        $latitude = $params['latitude'];
                        $longitude = $params['longitude'];
                        $distance = (!empty($params['searchRadius'])) ? $params['searchRadius'] : 125;
                        $locations = Location::having('distance', '<', $distance)
                            ->select(
                                \DB::raw(
                                    "*, (111.111 * DEGREES(
                                        ACOS(
                                            LEAST(
                                                1.0,
                                                COS(RADIANS($latitude))
                                                 * COS(RADIANS(latitude))
                                                 * COS(RADIANS($longitude - longitude))
                                                 + SIN(RADIANS($latitude))
                                                 * SIN(RADIANS(latitude))
                                            )
                                        )
                                    )) AS `distance`"
                                )
                            )
                            ->where($where)
                            ->orderBy('distance', 'ASC');
                    } else {
                        // Or get everything.
                        $locations = Location::where($where);
                    }

                    // Excluded by IDs
                    if (!empty($locationModel->excluded_ids)) {
                        $excludedIds = explode(',', preg_replace('/\s/', '', $locationModel->excluded_ids));
                        $models = $model->whereNotIn('id', $excludedIds)->get();
                    } else {
                        $models = $model->get();
                    }
                    $models = $models->keyBy('id');

                    $locations = $locations
                        ->whereIn('model_id', array_keys($models->toArray()))
                        ->limit($limit)
                        ->get();

                    if (!empty($locations)) {
                        /** @var Location $location */
                        foreach ($locations as $location) {
                            $location['model'] = $models[$location->model_id];
                            $output[$location->model_id] = $location;
                        }
                    }

                    return $output;
                });

                $model->addDynamicMethod('getLocation', function () use ($model) {
                    $location = Location::where('model_id', $model->id)->first();
                    return ($location !== null) ? $location->data : [];
                });

                // Fill 'location' fields with its data
                $model->bindEvent('model.afterFetch', function () use ($model) {
                    $model->location = $model->getLocation();
                });

                // Remove 'location' data from attributes to prevent it from trying to save it to the current model
                $model->bindEvent('model.beforeUpdate', function () use ($model) {
                    if (isset($model->attributes['location'])) {
                        unset($model->attributes['location']);
                    }
                });

                // Get the 'location' data and save it
                $model->bindEvent('model.afterCreate', function () use ($model, $locationModel) {
                    $location = $model->getLocation();

                    // Populate popup field when necessary
                    if (!empty($locationModel->popup_field)) {
                        $location['popup_content'] = $model->{$locationModel->popup_field};
                    }

                    // Maybe overwrite the location data with the fields of the current model
                    if (!empty($locationModel->overwrite_fields)) {
                        foreach ($locationModel->overwrite_fields as $field) {
                            $location[$field['type']] = $model->{$field['field']};
                        }
                    }

                    // Save the location data
                    Location::updateOrCreate(
                        ['model_id' => $model->id],
                        [
                            'model_type' => $locationModel->model_type,
                            'latitude' => $location['latitude'] ?? '',
                            'longitude' => $location['longitude'] ?? '',
                            'data' => $location,
                        ]
                    );
                });

                // Get the 'location' data and save it
                $model->bindEvent('model.afterUpdate', function () use ($model, $locationModel) {
                    try {
                        $runningInBackend = \App::runningInBackend();
                        // Only update location fields when updating from the backend
                        if (!$runningInBackend) {
                            return;
                        }

                        $className = (new \ReflectionClass($locationModel->model_type))->getShortName();
                        $postData = post($className);
                        $locationData = (!empty($postData['location'])) ? (array) $postData['location'] : [];

                        // Populate popup field when necessary
                        if (!empty($locationModel->popup_field)) {
                            $locationData['popup_content'] = $postData[$locationModel->popup_field];
                        }

                        // Maybe overwrite the location data with the fields of the current model
                        if (!empty($locationModel->overwrite_fields)) {
                            $currentLocation = $model->getLocation();
                            $original = $model->getOriginal();
                            foreach ($locationModel->overwrite_fields as $field) {
                                // Do not overwrite when the original location field has been changed
                                if (@$locationData[$field['type']] !== @$currentLocation[$field['type']]) {
                                    continue;
                                }
                                // Do not overwrite when the post data has been unchanged
                                if ($postData[$field['field']] == $original[$field['field']]) {
                                    continue;
                                }
                                $locationData[$field['type']] = $postData[$field['field']];
                            }
                        }

                        // Save the location data
                        Location::updateOrCreate(
                            ['model_id' => $model->id],
                            [
                                'model_type' => $locationModel->model_type,
                                'latitude' => $locationData['latitude'] ?? '',
                                'longitude' => $locationData['longitude'] ?? '',
                                'data' => $locationData,
                            ]
                        );
                    } catch (\ReflectionException $e) {
                        // Do nothing for now.
                    }
                });
            });
        }
    }
}
