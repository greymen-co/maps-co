<?php
namespace Greymen\MapsCo\Classes;

use Backend\Classes\Controller;
use Greymen\MapsCo\Models\Setting;
use Greymen\MapsCo\Models\LocationModel;
use Twig;
use Response;
use View;

class MapboxCoder extends Controller
{
    public $apiKey;
    public $address;
    public $lat;
    public $lng;
    public $resp;
    public $formattedAddress;

    public function __construct($addressParts=null)
    {
        $this->apiKey = Setting::get('mapbox_api_key');
        if($addressParts)
        {
            $this->buildAddress($addressParts);
            $this->geocode();
        }
    }

    public function buildAddress($addressParts)
    {
        $this->address = implode(
            ',', [
                $addressParts['address_1'],
                ', ' .$addressParts['city'],
                ', '. $addressParts['country'],
            ]
        );
    }

    public function geocode()
    {
        $url = 'https://api.mapbox.com/geocoding/v5/mapbox.places/' .
        rawurlencode($this->address).
        '.json?access_token=' . $this->apiKey;

        $resp = json_decode(file_get_contents($url), true);
        if (! isset($resp['features'][0])) return false;
//        dd(isset($resp['features'][0]));
        $this->resp                 = $resp;
        $this->lat                  = $this->resp['features'][0]['geometry']['coordinates'][1];
        $this->lng                  = $this->resp['features'][0]['geometry']['coordinates'][0];
        $this->formattedAddress     = $this->resp['features'][0]['place_name'];
        return true;
    }
    public function geoByZip($zip, $country='nl')
    {
        if(!$zip) return false;

 //       https://api.mapbox.com/geocoding/v5/mapbox.places/1338%20LW.json?country=nl&fuzzyMatch=false&autocomplete=false&types=postcode&access_token=TOKEN

        $url = 'https://api.mapbox.com/geocoding/v5/mapbox.places/' .
        rawurlencode($zip).
        '.json?country='.$country.'&fuzzyMatch=false&autocomplete=true&types=postcode&access_token=' . $this->apiKey;

        $resp = json_decode(file_get_contents($url), true);
        return $resp;
    }

    public function geoByCity($city)
    {
        if(!$city) return false;
        $city   = rawurlencode($city);
        $url    = "https://api.mapbox.com/geocoding/v5/mapbox.places/$city.json?access_token=".$this->apiKey."&autocomplete=true&types=place%2Cpostcode";
        $resp = json_decode(file_get_contents($url), true);
        return $resp;
    }

    public function getGeoJSON()
    {
        $data = LocationModel::getGeoJSON();
        // dd($data);
        // // $view =  $this->makePartial('plugins/greymen/mapsco/components/locationbyslug/popup');

        // $json = ($this->makePartial('plugins/greymen/mapsco/components/mapbox/geojson', ['data' => $data]));
        // dd($json);
        // // return Response::json($json);

        return Response::json(($data));
    }

}
