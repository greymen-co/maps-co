<?php namespace Greymen\MapsCo\Models;

use Model;

/**
 * Class Location
 * @package Greymen\MapsCo\Models
 */
class Location extends Model
{
    use \October\Rain\Database\Traits\Validation;

    public $fillable = ['model_id', 'model_type', 'latitude', 'longitude', 'data'];

    public $jsonable = ['data'];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'greymen_mapsco_locations';

    /**
     * @var array Validation rules
     */
    public $rules = [];
}
