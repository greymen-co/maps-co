<?php namespace Greymen\MapsCo\Models;

use Model;

/**
 * Class Setting
 * @package Greymen\MapsCo\Models
 */
class Setting extends Model {
    /** @var string[] */
    public $implement = ['System.Behaviors.SettingsModel'];

    /** @var string */
    public $settingsCode = 'greymen_mapsco_settings';

    /** @var string */
    public $settingsFields = 'fields.yaml';
}
