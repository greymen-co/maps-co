<?php namespace Greymen\MapsCo\Models;

use Model;
use Schema;

/**
 * Class LocationModel
 * @package Greymen\MapsCo\Models
 */
class LocationModel extends Model
{
    use \October\Rain\Database\Traits\SoftDelete;
    use \October\Rain\Database\Traits\Validation;

    public $jsonable = ['overwrite_fields'];

    protected $dates = ['deleted_at'];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'greymen_mapsco_location_models';

    public $rules = [
        'model_type' => 'required|unique:greymen_mapsco_location_models',
    ];

    protected $casts = [
        'data' => 'array',
    ];

    /**
     * @return array
     */
    public static function getModels(): array
    {
        $self = new self();
        $output = [];
        if (!Schema::hasTable($self->getTable())) {
            return $output;
        }
        $models = self::all(['id', 'model_type'])->all();
        foreach ($models as $model) {
            if (!class_exists($model->model_type)) {
                continue;
            }
            $output[$model->id] = $model->model_type;
        }
        return $output;
    }
}
